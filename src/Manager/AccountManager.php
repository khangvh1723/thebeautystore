<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class AccountManager
 * @package App\Manager
 */
class AccountManager {
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordHasherInterface
     */
    private $userPasswordHasherInterface;
    /**
     * CartManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordHasherInterface $userPasswordHasherInterface
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $this->entityManager = $entityManager;
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }
    public function changePassword($user, string $oldPassword, string $newPassword): bool
    {
        if ($this->userPasswordHasherInterface->isPasswordValid($user, $oldPassword)) {
            $encodedPassword = $this->userPasswordHasherInterface->hashPassword($user, $newPassword);
            $user->setPassword($encodedPassword);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}
