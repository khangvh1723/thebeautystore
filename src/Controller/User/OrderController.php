<?php

namespace App\Controller\User;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/user')]
#[IsGranted('ROLE_USER')]
class OrderController extends AbstractController
{
    #[Route('/order', name: 'user_order')]
    public function showOrderAction(ManagerRegistry $doctrine, Security $security): Response
    {
        $em = $doctrine->getManager();
        $orders = $em->getRepository('App\Entity\Order')->findBy(['user' => $security->getUser()]);
        return $this->render('user/order/index.html.twig', [
            'orders' => $orders,
        ]);
    }
    #[Route('/order/view/{order_id}', name: 'user_order_view')]
    public function showOrderItemAction(ManagerRegistry $doctrine, $order_id): Response
    {
        $em = $doctrine->getManager();
        $orderItems = $em->getRepository('App\Entity\OrderItem')->findBy(['orderRef'=> $order_id]);
        $order = $em->getRepository('App\Entity\Order')->findOneBy(['id'=> $order_id]);
        return $this->render('user/order/items.html.twig', [
            'orderItems' => $orderItems,
            'order' => $order
        ]);
    }
}
