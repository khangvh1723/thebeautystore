<?php

namespace App\Controller\User;

use App\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LandingPageController extends AbstractController
{
    #[Route('/', name: 'app_landing_page')]
    public function index(ManagerRegistry $doctrine): Response
    {   $em = $doctrine->getManager();
        $repo = $em->getRepository(Product::class);
        $products = $repo->findProductShop();
        return $this->render('user/landing_page/index.html.twig', [
            'products' => $products
        ]);
    }
}
