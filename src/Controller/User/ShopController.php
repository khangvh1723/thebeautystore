<?php

namespace App\Controller\User;

use App\Entity\Gallery;
use App\Entity\Product;
use App\Entity\Category;
use App\Form\AddToCartType;
use App\Repository\ProductRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\CartManager;

class ShopController extends AbstractController
{
    #[Route('/shop', name: 'app_shop')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();
        $repo = $em->getRepository(Product::class);
        $products = $repo->findProductShop();
        $categories = $doctrine
            ->getRepository('App\Entity\Category')
            ->findAll();
        return $this->render('user/shop/index.html.twig', [
            'products' => $products, 'categories' => $categories
        ]);
    }
    #[Route('/shop/details/{id}', name: 'shop_details')]
    public function showAction(ManagerRegistry $doctrine, Request $request, $id, Product $product,CartManager $cartManager): Response
    {
        $form = $this->createForm(AddToCartType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();
            $item->setProduct($product);

            $cart = $cartManager->getCurrentCart();
            $cart
                ->addItem($item)
                ->setUpdatedAt(new \DateTime());

            $cartManager->save($cart);

            return $this->redirectToRoute('shop_details', ['id' => $product->getId()]);
        }

        $product = $doctrine
            ->getRepository('App\Entity\Product')
            ->find($id);
        $em = $doctrine->getManager();
        $repo = $em->getRepository(Gallery::class);
        $galleries = $repo->findByProduct($id);
        return $this->render('user/shop/details.html.twig', [
            'product' => $product, 'galleries' => $galleries, 'form' => $form->createView()
        ]);
    }
    #[Route('/shop/{category_id}', name: 'shop_filter')]
    public  function filterProductAction(ManagerRegistry $doctrine ,$category_id):Response
    {
        $em = $doctrine->getManager();
        $repo = $em->getRepository(Product::class);
        $products = $repo->filterProductShop($category_id);
        $categories = $doctrine->getRepository('App\Entity\Category')->findAll();
        return $this->render('user/shop/index.html.twig', ['products' => $products,
            'categories'=>$categories]);
    }
}
