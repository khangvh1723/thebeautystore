<?php

namespace App\Controller\User;

use App\Entity\Order;
use App\Form\CartType;
use App\Manager\CartManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
#[IsGranted('ROLE_USER')]
class CartController extends AbstractController
{
    #[Route('/user/cart', name: 'app_user_cart')]
    public function index(CartManager $cartManager, Request $request): Response
    {
        $cart = $cartManager->getCurrentCart();
        $form = $this->createForm(CartType::class, $cart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cart->setUpdatedAt(new \DateTime());
            $cartManager->save($cart);

            return $this->redirectToRoute('app_user_cart');
        }

        return $this->render('user/cart/index.html.twig', [
            'cart' => $cart,
            'form' => $form->createView()
        ]);
    }
    #[Route('/user/checkout', name: 'app_user_checkout')]
    public function checkout(CartManager $cartManager): Response
    {
        // Retrieve the current cart
        $cart = $cartManager->getCurrentCart();
        $cartManager->checkout($cart);

        // Redirect to a success page or another relevant page
        return $this->redirectToRoute('app_user_cart');
    }
}
