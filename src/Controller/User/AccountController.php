<?php

namespace App\Controller\User;

use App\Form\UserType;
use App\Manager\AccountManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Form\ResetPasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/user')]
#[IsGranted('ROLE_USER')]
class AccountController extends AbstractController
{
    #[Route('/account/edit', name: 'edit_user')]
    public function editAction(ManagerRegistry $managerRegistry, Request $request, Security $security)
    {
        $user = $security->getUser();
        $user = $managerRegistry->getRepository('App\Entity\User')->find($user);
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $managerRegistry->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'notice',
                'User Edited'
            );
            return $this->redirectToRoute('edit_user');
        }
        return $this->renderForm('user/account/edit.html.twig', ['form' => $form]);
    }
    #[Route('/account/password/change', name: 'change_password')]
    public function changePassword(AccountManager $accountManager,Request $request)
    {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $oldPassword = $form->get('oldPassword')->getData();
            $newPassword = $form->get('newPassword')->getData();
            if ($accountManager->changePassword($user, $oldPassword, $newPassword)) {
                return $this->redirectToRoute('app_login');
            } else {
                $this->addFlash('error', 'Invalid old password');
            }
        }

        return $this->renderForm('login/reset_password.html.twig', [
            'form' => $form,
        ]);
    }
}
