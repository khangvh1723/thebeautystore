<?php

namespace App\Controller\Admin;

use App\Form\AdminAccountType;
use App\Manager\AccountManager;
use App\Form\ResetPasswordType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class AccountController extends AbstractController
{
    #[Route('/account/edit', name: 'edit_account')]
    public function edit(ManagerRegistry $managerRegistry, Request $request, Security $security): Response
    {
        $user = $security->getUser();
        $user = $managerRegistry->getRepository('App\Entity\Admin')->find($user);
        $form = $this->createForm(AdminAccountType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $managerRegistry->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'notice',
                'User Edited'
            );
            return $this->redirectToRoute('admin_edit_account');
        }
        return $this->renderForm('admin/account/edit.html.twig', ['form' => $form]);
    }
    #[Route('/account/password/change', name: 'change_password')]
    public function changeAdminPassword(AccountManager $accountManager, Request $request)
    {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $oldPassword = $form->get('oldPassword')->getData();
            $newPassword = $form->get('newPassword')->getData();

            if ($accountManager->changePassword($user, $oldPassword, $newPassword)) {
                return $this->redirectToRoute('app_login');
            } else {
                $this->addFlash('error', 'Invalid old password');
            }
        }
                return $this->renderForm('admin/account/reset_password.html.twig', [
            'form' => $form,
        ]);
    }
}
