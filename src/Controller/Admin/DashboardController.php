<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class DashboardController extends AbstractController
{
    #[Route('/dashboard', name: 'app_dashboard')]
    public function index(ManagerRegistry $managerRegistry): Response
    {
        $countProduct = $managerRegistry->getRepository('App\Entity\Product')->countProduct();
        $countUser = $managerRegistry->getRepository('App\Entity\User')->countUser();
        $countOrder = $managerRegistry->getRepository('App\Entity\Order')->countOrder();
        $countCompletedOrder = $managerRegistry->getRepository('App\Entity\Order')->countCompletedOrder();
        return $this->render('admin/dashboard/index.html.twig', [
            'countProduct' => $countProduct,
            'countOrder' => $countOrder,
            'countUser' => $countUser,
            'countCompletedOrder' => $countCompletedOrder,
        ]);
    }
}
