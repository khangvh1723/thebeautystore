<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class UserController extends AbstractController
{
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/user', name: 'app_user')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $em = $doctrine->getManager();
        $repo = $em->getRepository(User::class);
        $users = $doctrine->getRepository('App\Entity\User')->findAll();
        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
        ]);
    }
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/user/edit/{id}', name: 'edit_user')]
    public function editAction(ManagerRegistry $doctrine, $id, Request $request)
    {
        $em = $doctrine->getManager();
        $user = $em->getRepository('App\Entity\User')->find($id);
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $doctrine->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'notice',
                'User Edited'
            );
            return $this->redirectToRoute('admin_app_user');
        }
        return $this->renderForm('admin/user/edit.html.twig', ['form' => $form, 'id' => $id]);
    }
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/user/delete/{id}', name: 'delete_user')]
    public function deleteAction(ManagerRegistry $doctrine, $id)
    {
        $em = $doctrine->getManager();
        $product = $em->getRepository('App\Entity\User')->find($id);
        $em->remove($product);
        $em->flush();

        $this->addFlash(
            'error',
            'User deleted'
        );
        return $this->redirectToRoute('admin_app_user');
    }
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/user/reset/{id}', name: 'reset_user')]
    public function resetPasswordAction(ManagerRegistry $managerRegistry, $id, UserPasswordHasherInterface $userPasswordHasher)
    {
        $em = $managerRegistry->getManager();
        $user = $em->getRepository('App\Entity\User')->find($id);
        $user->setPassword(
            $userPasswordHasher->hashPassword(
                $user, '123456'
            )
        );
        $em = $managerRegistry->getManager();
        $em->persist($user);
        $em->flush();
        $this->addFlash(
            'notice',
            'Reset password successfully. User id: '.$user
        );

        return $this->redirectToRoute('admin_app_user');
    }
}
