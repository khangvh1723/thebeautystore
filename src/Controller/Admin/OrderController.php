<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Form\AdminCartType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class OrderController extends AbstractController
{
    #[Route('/order', name: 'app_order')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $orders = $doctrine->getRepository('App\Entity\Order')->findAll();
        return $this->render('admin/order/index.html.twig', [
            'orders' => array_reverse($orders),
        ]);
    }
    #[Route('/order/view/{id}', name: 'order_items')]
    public function viewOrderItemsAction(ManagerRegistry $doctrine, $id): Response
    {
        $items = $doctrine->getRepository('App\Entity\OrderItem')->findBy(['orderRef'=>$id]);
        $order = $doctrine->getRepository('App\Entity\Order')->find($id);
        return $this->render('admin/order/orderItems.html.twig', [
            'items' => $items,
            'order' => $order
        ]);
    }
    #[Route('/order/status/{id}', name: 'order_status')]
    public function setOrderStatusAction(ManagerRegistry $doctrine, $id): Response
    {
        $orders = $doctrine->getRepository('App\Entity\Order')->findAll();
        $em = $doctrine->getManager();
        $order = $em->getRepository('App\Entity\Order')->find($id);
        $form = $this->createForm(AdminCartType::class, $order);
        $em = $doctrine->getManager();
        $order->setStatus(Order::STATUS_COMPLETE);
        $em->persist($order);
        $em->flush();

        $this->addFlash(
            'notice',
            'Complete Order'
        );
        return $this->redirectToRoute('admin_app_order');
    }
}
