<?php

namespace App\Controller\Admin;

use App\Entity\Admin;
use App\Form\AdminType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class AdminController extends AbstractController
{
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin', name: 'app_admin')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $admins = $doctrine->getRepository('App\Entity\Admin')->findAll();
        return $this->render('admin/admin/index.html.twig', [
            'admins' => $admins,
        ]);
    }
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin/create', name: 'create_admin', methods: ['GET', 'POST'])]
    public function createAction(ManagerRegistry $doctrine, Request $request, UserPasswordHasherInterface $userPasswordHasher)
    {
        $admin = new Admin();
        $form = $this->createForm(AdminType::class, $admin);
        $admin->setRoles(array('ROLE_ADMIN'));
        $admin->setPassword(
            $userPasswordHasher->hashPassword(
                $admin, '123456'
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $doctrine->getManager();
            $em->persist($admin);
            $em->flush();

            $id = $admin->getId();

            $this->addFlash(
                'notice',
                'Admin Added'
            );
            return $this->redirectToRoute('admin_app_admin');
        }
        return $this->renderForm('admin/admin/create.html.twig', ['form' => $form]);
    }
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin/delete/{id}', name: 'delete_admin')]
    public function deleteAction(ManagerRegistry $doctrine, $id)
    {
        $em = $doctrine->getManager();
        $product = $em->getRepository('App\Entity\Admin')->find($id);
        $em->remove($product);
        $em->flush();

        $this->addFlash(
            'error',
            'Admin deleted'
        );
        return $this->redirectToRoute('admin_app_admin');
    }
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin/edit/{id}', name: 'edit_admin')]
    public function editAction(ManagerRegistry $doctrine, $id, Request $request)
    {
        $em = $doctrine->getManager();
        $admin = $em->getRepository('App\Entity\Admin')->find($id);
        $form = $this->createForm(AdminType::class, $admin);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $doctrine->getManager();
            $em->persist($admin);
            $em->flush();

            $this->addFlash(
                'notice',
                'Admin Edited'
            );
            return $this->redirectToRoute('admin_app_admin');
        }
        return $this->renderForm('admin/admin/edit.html.twig', ['form' => $form, 'id' => $id]);
    }
    #[IsGranted('ROLE_SUPER_ADMIN')]
    #[Route('/admin/reset/{id}', name: 'reset_admin')]
    public function resetPasswordAction(ManagerRegistry $managerRegistry, $id, UserPasswordHasherInterface $userPasswordHasher)
    {
        $em = $managerRegistry->getManager();
        $admin = $em->getRepository('App\Entity\Admin')->find($id);
        $admin->setPassword(
            $userPasswordHasher->hashPassword(
                $admin, '123456'
            )
        );
        $em = $managerRegistry->getManager();
        $em->persist($admin);
        $em->flush();

        $this->addFlash(
            'notice',
            'Reset password successfully. Admin id: '.$admin
        );

        return $this->redirectToRoute('admin_app_admin');
    }
}
