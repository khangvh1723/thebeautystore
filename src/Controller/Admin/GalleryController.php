<?php

namespace App\Controller\Admin;

use App\Entity\Gallery;
use App\Form\GalleryType;
use App\Repository\GalleryRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_ADMIN')]
#[Route('/admin', name: 'admin_')]
class GalleryController extends AbstractController
{
    #[Route('/gallery/create/{id}', name: 'create_gallery', methods: ['GET', 'POST'])]
    public function createAction(ManagerRegistry $doctrine, Request $request, SluggerInterface $slugger, $id)
    {
        // render galleries regarding product
        $product = $doctrine
            ->getRepository('App\Entity\Product')
            ->find($id);
        $em = $doctrine->getManager();
        $repo = $em->getRepository(Gallery::class);
        $galleries = $repo->findByProduct($id);

        $gallery = new Gallery();
        $form = $this->createForm(GalleryType::class, $gallery);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // upload file
            $thumbnail = $form->get('thumbnail')->getData();
            if ($thumbnail) {
                $originalFilename = pathinfo($thumbnail->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $thumbnail->guessExtension();
                // Move the file to the directory where brochures are stored
                try {
                    $thumbnail->move(
                        $this->getParameter('thumbnail_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    $this->addFlash(
                        'error',
                        'Cannot upload'
                    );
                }
                $gallery->setThumbnail($newFilename);
            } else {
                $this->addFlash(
                    'error',
                    'Cannot upload'
                );
                /**handle exception if something happens during file uploade */
            }
            $em = $doctrine->getManager();
            $em->persist($gallery);
            $em->flush();

            $this->addFlash(
                'notice',
                'Gallery Added'
            );
            return $this->redirectToRoute('admin_create_gallery', ['id' => $id]);
        }
        return $this->renderForm('/admin/gallery/create.html.twig', ['form' => $form, 'product' => $product, 'galleries' => $galleries]);
    }
    #[Route('/gallery/delete/{product_id}/{id}', name: 'delete_gallery')]
    public function deleteAction(ManagerRegistry $doctrine, $product_id, $id)
    {
        $em = $doctrine->getManager();
        $gallery = $em->getRepository('App\Entity\Gallery')->find($id);
        $em->remove($gallery);
        $em->flush();

        $this->addFlash(
            'error',
            'Gallery deleted'
        );
        //        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
        return $this->redirectToRoute('admin_create_gallery', ['id' => $product_id]);
    }
}
