$(document).ready(function(){

    var quantity = 0;
    $('.quantity-plus').click(function(e){

        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#add_to_cart_quantity').val());

        // If is not undefined
        if(quantity<10) {
            $('#add_to_cart_quantity').val(quantity + 1);
        }


        // Increment

    });

    $('.quantity-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#add_to_cart_quantity').val());

        // If is not undefined

        // Increment
        if(quantity>1){
            $('#add_to_cart_quantity').val(quantity - 1);
        }
    });

});