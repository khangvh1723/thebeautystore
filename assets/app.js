/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import 'bootstrap';
import './app.scss';
import './styles/fontawesome-free-6.4.0-web/css/all.min.css';
import getNiceMessage from './js/get_nice_message';
import $ from 'jquery';

// start the Stimulus application
import './bootstrap';
console.log(getNiceMessage(5));
